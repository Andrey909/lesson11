<?php
class News extends Publication{
    protected $source;

    public function __construct($title, $shortText, $fullText,$source)
    {
        parent::__construct($title, $shortText, $fullText);
        $this->source=$source;
    }

    public function getShortPreview(){
        return parent::getShortPreview().$this->source;
    }
}
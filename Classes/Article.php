<?php
class Article extends Publication{
    protected $author;

    /**
     * News constructor.
     */
    public function __construct($title, $shortText, $fullText, $author)
    {
        parent::__construct($title, $shortText, $fullText);
        $this->author=$author;
    }

    public function getShortPreview(){
        return parent::getShortPreview().$this->author;
    }
}
<?php

class PublicationsWriter
{
    public $publications = [];

    /**
     * PublicationsWriter constructor.
     */
    public function __construct($type, $pdo)
    {
        $query = "SELECT * FROM publication WHERE type=$type";
        $res = $pdo->query($query);
        $publicationArr = $res->fetchObject();

        if ($type == 'News') {
            foreach ($publicationArr as $publication) {
                $this->publications[] = new News($publication['title'], $publication['shortText'], $publication['fullText'], $publication['source']);
            }
        }
        if ($type == 'Article') {
            foreach ($publicationArr as $publication) {
                $this->publications[] = new Article($publication['title'], $publication['shortText'], $publication['fullText'], $publication['author']);
            }
        }
    }
}
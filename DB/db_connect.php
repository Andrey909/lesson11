<?php
try{
    $pdo = new PDO('mysql:host=localhost;dbname=information',
        'root','');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec("SET NAMES 'utf8'");

}catch (PDOException $exception){
    $message = 'Не удалось подключиться к БД' . $exception->getMessage();
    die($message);
}
<?php
header('Content-Type: text/html; charset=utf-8');
require_once 'db_connect.php';
try{
	$sqlQuery = '
		CREATE TABLE publication(
			id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			title VARCHAR(255),
			shortText TEXT,
			fullText TEXT,
			type VARCHAR(255),
			author VARCHAR(255),
			source VARCHAR(255),
		)';
	$mydb->exec($sqlQuery);
}catch(PDOException $e){
	die('Не удалось создать таблицу members!<br>'.$e->getMessage());
}

